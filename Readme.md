# Configurando Git
## Hoja03_Markdown_02
Clonar un repositorio existente:
~~~
$ git clone https://gitlab.com/alejandro.ostuagarcia/alejandroostua_markdown.git
~~~
---
Añadimos los archivos, pero no lo confirmamos:
~~~
$ git add .
~~~
![git status de git add](imagenes/git_add.png)
---
Confirmamos los cambios:
~~~
$ git commit -m "Primer commit de Alex"
~~~
![git status de git commit](imagenes/git_commit.png)
---
Lo subimos al repositorio remoto:
~~~
$ git push
~~~
Para ignorar un archivo con git:
~~~
$ git ignore ///////
~~~

## Hoja03_Markdown_03
Creamos una nueva rama llamada "rama-Alejandro"
~~~
$ git branch rama-Alejandro
~~~

Cambiamos de rama, a "rama-Alejandro
~~~
$ git checkout rama-Alejandro
~~~

Subimos los cambios:
~~~
$ git add despliegue.md
$ git commit -m "Añadiendo el archivo despliegue.md en la rama-Alejandro"
$ git push origin rama-Alejandro
~~~

Nos posicionamos en la rama master:
~~~
$ git checkout master
~~~

Fusionamos las ramas:
~~~
$ git merge rama-Alejandro
Updating e061f8f..f9da74b
Fast-forward
 despliegue.md | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 despliegue.md
~~~
>fusion correcta

Ahora vamos a hacer una fusion o merge con conflicto
~~~
DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (rama-Alejandro)
$ git checkout master
Switched to branch 'master'
M       Readme.md
D       imagenes/git_status.png
Your branch is up to date with 'origin/master'.

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git merge rama-Alejandro
Updating e061f8f..f9da74b
Fast-forward
 despliegue.md | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 despliegue.md

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git add .

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git commit -m "primera evaluacion"
[master eb59884] primera evaluacion
 3 files changed, 49 insertions(+), 2 deletions(-)
 delete mode 100644 imagenes/git_status.png

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git push origin master
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 4 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 1.01 KiB | 1.01 MiB/s, done.
Total 5 (delta 1), reused 0 (delta 0)
To https://gitlab.com/alejandro.ostuagarcia/alejandroostua_markdown.git
   e061f8f..eb59884  master -> master

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git checkout rama-Alejandro
Switched to branch 'rama-Alejandro'

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (rama-Alejandro)
$ git add .

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (rama-Alejandro)
$ git commit -m "2evaluacion"
[rama-Alejandro 44ed62c] 2evaluacion
 1 file changed, 3 insertions(+)

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (rama-Alejandro)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git merge rama-Alejandro
Auto-merging despliegue.md
CONFLICT (content): Merge conflict in despliegue.md
Automatic merge failed; fix conflicts and then commit the result.
DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master|MERGING)
$ git add .

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master|MERGING)
$ git commit -m "merge arreglado"
[master 8ea5ce7] merge arreglado

DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git push origin master
Enumerating objects: 13, done.
Counting objects: 100% (13/13), done.
Delta compression using up to 4 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (7/7), 1.78 KiB | 1.78 MiB/s, done.
Total 7 (delta 1), reused 0 (delta 0)
To https://gitlab.com/alejandro.ostuagarcia/alejandroostua_markdown.git
   eb59884..8ea5ce7  master -> master
~~~
> si hay conflicto lo arreglamos con visual studio o a mano en este proceso he creado en un mismo archivo
dos lineas diferentes y las he fusionado, he elegido la opcion de guardar los dos cambios

creamos un tag y borramos la rama anterior
~~~
DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git tag v0.2


DAW223@IF01-10 MINGW64 ~/Desktop/DAW/GIT/alejandroostua_markdown (master)
$ git branch -d rama-Alejandro
Deleted branch rama-Alejandro (was 44ed62c).

~~~

